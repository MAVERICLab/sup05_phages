#!/usr/bin/perl
use strict;
# Script to detect contigs with viral hallmark genes
# Argument 0 : csv file with gene affiliation
# Argument 1 : out csv file

if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[1])))
{
	print "# Script to detect contigs with viral hallmark genes
# Argument 0 : csv file with gene affiliation
# Argument 1 : out csv file\n";
	die "\n";
}


my $csv_file=$ARGV[0];
my $out_file=$ARGV[1];

# Files available through Metavir (http://metavir-meb.univ-bpclermont.fr) : Project 2948
my $fasta_base="2948_whole.fasta";
my $fasta_base_prot="2948_prots.fasta";
my $circu_list="2948_circu.list";

# This gene has "capsid" in its name but yield false positive detection as several bacterial protease are similar in sequence to this viral protease, so we don't consider these hits
my %keywords_to_remove=("bacteriophage Clp protease involved in capsid processing"=>1);

# Get size, number of proteins, and circularity information of all contigs
open(FA,"<$fasta_base") || die "pblm opening $fasta_base\n";
my $id_c="";
my %size;
while(<FA>){
	chomp($_);
	if ($_=~/^>(.*)/){
		$id_c=$1;
	}
	else{
		$size{$id_c}+=length($_);
	}
}
close FA;

open(PROT,"<$fasta_base_prot") || die "pblm opening $fasta_base_prot\n";
my %nb_prot;
while(<PROT>){
	chomp($_);
	if ($_=~/^>\d*_(.*)-gene_.*/){
		my $id=$1;
		$nb_prot{$id}++;
	}
}
close PROT;

my %check_circu;
open(CIRCU,"<$circu_list") || die "pblm ouverture fichier $circu_list\n";
while(<CIRCU>){
	chomp($_);
	if ($_=~/^>\d*_(.*)/){
		my $id=$1;
		$check_circu{$id}=1;
	}
}
close CIRCU;

my %infos;
my %liste_contigs;
# Get the automatic annotation from Metavir
open(F1,"<$csv_file") || die "pblm ouverture fichier $csv_file\n";
while(<F1>){
	chomp($_);
	if ($_=~/^Cell\|Contig/){}
	else{
		my @tab=split(/\|/,$_);
		if (defined($tab[8])){
 			print "!!!!!!!!!!!!!!!!!!! pblm with line $_\n";
			<STDIN>;
		}
		if (!defined($infos{$tab[0]}{$tab[1]}{$tab[2]})){
			my @temp=($tab[7]);
			$infos{$tab[0]}{$tab[1]}{$tab[2]}{"product"}=\@temp;
			my @temp2=($tab[6]);
			$infos{$tab[0]}{$tab[1]}{$tab[2]}{"taxo"}=\@temp2;
			$infos{$tab[0]}{$tab[1]}{$tab[2]}{"best_hit"}{"id"}=$tab[4];
			$infos{$tab[0]}{$tab[1]}{$tab[2]}{"best_hit"}{"score"}=$tab[3];
			$infos{$tab[0]}{$tab[1]}{$tab[2]}{"best_hit"}{"taxo"}=$tab[6];
		}
		else{
			push(@{$infos{$tab[0]}{$tab[1]}{$tab[2]}{"product"}},$tab[7]);
			push(@{$infos{$tab[0]}{$tab[1]}{$tab[2]}{"taxo"}},$tab[6]);
			if ($tab[3]>$infos{$tab[0]}{$tab[1]}{$tab[2]}{"best_hit"}){
				$infos{$tab[0]}{$tab[1]}{$tab[2]}{"best_hit"}{"id"}=$tab[4];
				$infos{$tab[0]}{$tab[1]}{$tab[2]}{"best_hit"}{"score"}=$tab[3];
				$infos{$tab[0]}{$tab[1]}{$tab[2]}{"best_hit"}{"taxo"}=$tab[6];
			}
		}
	}
}
close F1;

my $seuil_nb_hits_viraux=5;
my $size_threshold=15000;
# Summarize the results in a tab file
open(S1,">$out_file") || die "pblm opening file $out_file\n";
print S1 "project|contig|contig size|circular|nb genes|class|nb viral hits|lca|best_hit|hallmark genes number|hallmark genes list\n";
foreach(keys %infos){
	my $project=$_;
	foreach(keys %{$infos{$project}}){
		my $contig=$_;
		my $check_contig=0;
		my @liste_gene=keys %{$infos{$project}{$contig}};
		my $n=$#liste_gene+1;
		if ($n>$seuil_nb_hits_viraux){$check_contig=1;}
		my $hallmark="";
		my $hallmark_number=0;
		my %vu_hallmark;
		my @best_hit_tables;
		foreach(@liste_gene){
			my @tab_product=@{$infos{$project}{$contig}{$_}{"product"}};
			my $tag=0;
			foreach(@tab_product){
				# check for viral hallmark genes
				if ((($_=~/.*capsid.*/) || ($_=~/.*terminase.*/) || ($_=~/.*portal.*/) || ($_=~/.*tail.*/) || ($_=~/.*coat.*/) || ($_=~/.*virion.*/) || ($_=~/.*spike.*/)) && (!defined($keywords_to_remove{$_}))){
					if ($check_contig<2){$check_contig+=2;}
					if (!defined($vu_hallmark{$_})){
						$hallmark.=$_." ; ";
						$vu_hallmark{$_}=1;
					}
					if ($tag==0){$hallmark_number++;$tag=1;}
				}
			}
		}
		my @best_hit_table=sort { $infos{$project}{$contig}{$b}{"best_hit"}{"score"} <=>  $infos{$project}{$contig}{$a}{"best_hit"}{"score"}  } keys %{$infos{$project}{$contig}};
		my $best_hit_base=$infos{$project}{$contig}{$best_hit_table[0]}{"best_hit"}{"taxo"};
		my @ref=split(";",$infos{$project}{$contig}{$best_hit_table[0]}{"best_hit"}{"taxo"});
		for (my $i=1;$i<5;$i++){
			if (defined($best_hit_table[$i])){
				my @new_ref;
				my @tab_taxo_temp=split(";",$infos{$project}{$contig}{$best_hit_table[$i]}{"best_hit"}{"taxo"});
				for (my $j=0;$j<=$#ref;$j++){
					if ($ref[$j]=~/.*unclassified.*/){
						push(@new_ref,$tab_taxo_temp[$j]);
					}
					if (($tab_taxo_temp[$j]=~/.*unclassified.*/) || ($tab_taxo_temp[$j] eq $ref[$j])){
						push(@new_ref,$ref[$j]);
					}
					else{
# 						print "pblm niveau $j : $tab_taxo_temp[$j] et $ref[$j]\n";
						$j=$#ref+1;
					}
				}
				@ref=@new_ref;
			}
			
		}
		my $lca=join(";",@ref);
		if (!defined($size{$contig})){
			print "!!! pblm $contig -> pas de taille\n";
			<STDIN>;
		}
		elsif(!defined($nb_prot{$contig})){
			print "§§§§ pblm $contig -> pas de nb prot\n";
			<STDIN>;
		}
		if (($check_contig>=2) && (($size{$contig}>$size_threshold) || ($check_circu{$contig}==1))){
			print S1 "$project|$contig|$size{$contig}|$check_circu{$contig}|$nb_prot{$contig}|$check_contig|$n|$lca|$best_hit_base|$hallmark_number|$hallmark\n";
		}
	}
}

close S1;
