#!/usr/bin/perl
use strict;
# Script to asses metrics on viral and microbial contigs to check differences in terms of frequency of strand switch, gene size, and ratio of uncharacterized genes
# Argument 0 : csv file with each gene affiliation
# Argument 1 : summary table of viral and non-microbial affilation
# Argument 2 : out file

if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[2])))
{
	print "# Script to asses metrics on viral and microbial contigs
# Argument 0 : csv file with each gene affiliation
# Argument 1 : summary table of viral and non-microbial affilation
# Argument 2 : out file\n";
	die "\n";
}

my $csv_file=$ARGV[0];
my $affi_tab=$ARGV[1];
my $out_file=$ARGV[2];

## We will check these metrics if and only if there are more than 10 genes predicted on the contig, except for the vira contig for which we take every contig
my $seuil_genes=10;

# We get affiliation for each contig
my %affi;
open(AF,"<$affi_tab") || die ("pblm opening file $affi_tab\n");
while (<AF>){
	chomp($_);
	my @tab=split("\t",$_);
	$affi{$tab[0]}=$tab[1];
}
close AF;

my %infos;
my $gene_num;
my $contig_c;
# Getting gene annotation
open(F1,"<$csv_file") || die "pblm ouverture fichier $csv_file\n";
while(<F1>){
	chomp($_);
	if ($_=~/^Contig/){}
	elsif($_=~/^,/){
		my @tab=split(',',$_);
		if (!defined($tab[10])){
			print "!!!!!!!!!!!!!!!!!!! pblm with line $_\n";
			<STDIN>;
		}
		$tab[10]=~s/\"//g;
		$tab[5]=~s/\"//g;
		$infos{$contig_c}{$tab[1]}{"order"}=$gene_num;
		$infos{$contig_c}{$tab[1]}{"size"}=$tab[2];
		$infos{$contig_c}{$tab[1]}{"sens"}=$tab[5];
		$infos{$contig_c}{$tab[1]}{"product"}=$tab[10];
		$gene_num++;
	}
	else{
		$_=~/\d*_(.*)/;
		$contig_c=$1;
		$gene_num=0;
	}
}
close F1;


# Summary of the results
open(S1,">$out_file") || die ("pblm opening file $out_file\n");
print S1 "Contig_id,Affi,Ratio_unch,Avg_g_size,Ratio_strand_switch\n";
foreach(keys %infos){
	my $contig_c=$_;
	if (!defined($affi{$contig_c})){$affi{$contig_c}="bacterial";}
	my $avg_g_s=0;
	my $n_g=0;
	my $n_unch=0;
	my $nb_changes=-1;
	my $c_sens="";
	# Couting the number of uncharacterized genes, number of strand switch, and getting an average gene size for each contig
	foreach(sort {$infos{$contig_c}{$a}{"order"} <=> $infos{$contig_c}{$b}{"order"}} keys %{$infos{$contig_c}}){
		$n_g++;
		if ($infos{$contig_c}{$_}{"product"} eq "- -"){$n_unch++;}
		$avg_g_s+=$infos{$contig_c}{$_}{"size"};
		if ($infos{$contig_c}{$_}{"sens"} ne $c_sens){
			$nb_changes++;
			$c_sens=$infos{$contig_c}{$_}{"sens"};
		}
		
	}
	my $ratio_change=$nb_changes/$n_g;
	my $ratio_unch=$n_unch/$n_g;
	$avg_g_s/=$n_g;
	if ($n_g>$seuil_genes || $affi{$contig_c} eq "Viral"){
		print S1 "$contig_c,$affi{$contig_c},$ratio_unch,$avg_g_s,$ratio_change\n";
	}
}
close S1;