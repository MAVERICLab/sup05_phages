#!/usr/bin/perl
use strict;
# Script to generate the files for a recruitment plot against a contig
# Argument 0 : Contig code
# Argument 1 : Map file of the contig
# Argument 2 : Out file
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[2])))
{
	print "# Script to generate the files for a recruitment plot agains a SAG
# Argument 0 : contig code
# Argument 1 : map file
# Argument 2 : out file\n";
	die "\n";
}

# Size of the viral contigs used in the recruitment plots
my %contig_size=("AB_750C22AB_904_13_0"=>21553,"AB_750K04AB_904_0_0_part1"=>50302,"AB_750K04AB_904_0_0"=>50302,"AB_755_M08F06_0_0"=>52692,"AB_755_D02C10_21_0"=>4578,"AB_751_G10AB_905_6_0"=>27836);

# Get the number of reads associated with each contig in the metagenomes
my $depth_file="Read_depth_LineP.lst";
my %depth;
open(DEPTH,"<$depth_file") || die ("pblm opening file $depth_file\n");
while(<DEPTH>){
	chomp($_);
	my @tab=split("\t",$_);
	$depth{$tab[0]}=$tab[2];
}
close DEPTH;

# Absolute thresholds : Will only consider BLAST hits with at least 50 bit score, 40% of identity, an e-value lower than 0.001
my $score_th=50;
my $pcent_th=40;
my $evalue_th=0.001;

my $assoc_file="Assoc_viromes_genomes.lst";
my %type_dataset;

my $contig_code=$ARGV[0];
my $map_file=$ARGV[1];
my $out_file=$ARGV[2];


# List of the different BLAST to mine, the first 5 are BLASTp of contigs from metagenomes against all contigs in SAGs, the last three are tBLASTx of metagenomic reads (for smaller 454-sequences metagenomes) against all contigs in SAGs
my $blast_file_photic="Pool_blastp_prots_vs_Sup05_photiczone_nett.tab";
my $blast_file_aphotic="Pool_blastp_prots_vs_Sup05_aphoticzone_nett.tab";
my $blast_file_omz="Pool_blastp_prots_vs_Sup05_OMZ_nett.tab";
my $blast_file_microbial_metagenome="../Blasts_for_Recruitment_OMZ_vs_ocean/Non-viromes/Pool_prots_from_Hallam_Metagenomes_vs_Sup05_viruses_predicted_proteins_nett.tab";
my $blast_file_tblastx_photic="../Blasts_for_Recruitment_OMZ_vs_ocean/Pools/Pool_tblastx_reads_photiczone_vs_Sup05_nett.tab";
my $blast_file_tblastx_aphotic="../Blasts_for_Recruitment_OMZ_vs_ocean/Pools/Pool_tblastx_reads_aphoticzone_vs_Sup05_nett.tab";
my $blast_file_tblastx_omz="../Blasts_for_Recruitment_OMZ_vs_ocean/Pools/Pool_tblastx_reads_OMZ_vs_Sup05_nett.tab";

my $out_file_coverage=$out_file;
$out_file_coverage=~s/\.ggplot/_coverage.ggplot/;

my @heck;
my $tag=0;
# Check which type of metgenome we want to compute
open(ASSOC,"<$assoc_file") || die ("pblm opening file $assoc_file\n");
while (<ASSOC>){
	chomp($_);
	my @tab=split("\t",$_);
	push(@heck,$tab[0]);
	if ($contig_code eq $tab[0]){$tag=1;}
	
}
close ASSOC;


if ($tag==0){
	print "$contig_code unknown, here is the list of codes available : @heck\n";
	die("sorry\n");
}

my @liste_category=("photic","aphotic","OMZ_Saanich","OMZ_other");
print "We will only keep @liste_category\n";

my %coverage;
for(my $i=1;$i<=$contig_size{$contig_code};$i++){
	foreach(@liste_category){
		$coverage{$i}{$_}=0;
	}
}
# Read the map file to get gene coordinates
my %coord_prot;
open(MAP,"<$map_file") || die "fail opening map file $map_file\n";
while(<MAP>){
	chomp($_);
	my @tab=split(",",$_);
	my $id=$tab[0];
	my $start=$tab[1];
	my $stop=$tab[2];
	my $strand=$tab[6];
	$id=~/^\d*_(.*)_(gene_\d*).*/;
	my $id_contig=$1;
	my $temp=$2;
	if ($id_contig=~/(.*)_part1$/){
		$id_contig=$1;
	}
	my $id_prot=$id_contig."-".$temp;
	$coord_prot{$id_prot}{"start"}=$start;
	$coord_prot{$id_prot}{"stop"}=$stop;
	$coord_prot{$id_prot}{"strand"}=$strand;
	print "$id_prot de $id_contig ($id) start en $start et stop en $stop sur le brin $strand\n";
}
close MAP;

my %check;

# Parse all BLASTs
my %hits;
my $i=0;
open(TAB,"<$blast_file_photic") || die "pblm ouverture fichier $blast_file_photic\n";
while(<TAB>){
	chomp($_);
	my @tab=split("\t",$_);
	my $id_pcent=$tab[2];
	my $e_value=$tab[10];
	my $score=$tab[11];
	if (($score>$score_th) && ($e_value<$evalue_th) && ($id_pcent>$pcent_th)){
 		$tab[1]=~/^\d*_(.*)_(gene_\d*)/;
		my $id_contig=$1;
		if ($id_contig=~/(.*)_part1$/){
			$id_contig=$1;
		}
		my $id_prot=$1."-".$2;
		if (($id_contig eq $contig_code) && (defined($coord_prot{$id_prot}))){
			$i++;
			my $id="photic";
			my $id_read=$tab[0];
			$hits{$id}{$i}{"id_read"}=$id_read;
			$hits{$id}{$i}{"id_pcent"}=$tab[2];
			if (!defined($check{$id})){
				print "we check $id \n";
				$check{$id}=1;
			}
			if ($coord_prot{$id_prot}{"strand"}==1){
				$hits{$id}{$i}{"start"}=$coord_prot{$id_prot}{"start"}+($tab[8]*3);
				$hits{$id}{$i}{"stop"}=$coord_prot{$id_prot}{"start"}+($tab[9]*3);
			}
			elsif ($coord_prot{$id_prot}{"strand"}==-1){
				$hits{$id}{$i}{"start"}=$coord_prot{$id_prot}{"stop"}-($tab[9]*3);
				$hits{$id}{$i}{"stop"}=$coord_prot{$id_prot}{"stop"}-($tab[8]*3);
			}
		}
	}
}
close TAB;
print "photic done\n";

open(TAB,"<$blast_file_aphotic") || die "pblm ouverture fichier $blast_file_aphotic\n";
while(<TAB>){
	chomp($_);
	my @tab=split("\t",$_);
	my $id_pcent=$tab[2];
	my $e_value=$tab[10];
	my $score=$tab[11];
	if (($score>$score_th) && ($e_value<$evalue_th) && ($id_pcent>$pcent_th)){
 		$tab[1]=~/^\d*_(.*)_(gene_\d*)/;
		my $id_contig=$1;
		if ($id_contig=~/(.*)_part1$/){
			$id_contig=$1;
		}
		my $id_prot=$1."-".$2;
		if (($id_contig eq $contig_code) && (defined($coord_prot{$id_prot}))){
			$i++;
			my $id="aphotic";
			my $id_read=$tab[0];
			$hits{$id}{$i}{"id_read"}=$id_read;
			$hits{$id}{$i}{"id_pcent"}=$tab[2];
			if (!defined($check{$id})){
				print "on check $id \n";
				$check{$id}=1;
			}
			if ($coord_prot{$id_prot}{"strand"}==1){
				$hits{$id}{$i}{"start"}=$coord_prot{$id_prot}{"start"}+($tab[8]*3);
				$hits{$id}{$i}{"stop"}=$coord_prot{$id_prot}{"start"}+($tab[9]*3);
			}
			elsif ($coord_prot{$id_prot}{"strand"}==-1){
				$hits{$id}{$i}{"start"}=$coord_prot{$id_prot}{"stop"}-($tab[9]*3);
				$hits{$id}{$i}{"stop"}=$coord_prot{$id_prot}{"stop"}-($tab[8]*3);
			}
		}
	}
}
close TAB;
print "aphotic done\n";

open(TAB,"<$blast_file_omz") || die "pblm ouverture fichier $blast_file_omz\n";
while(<TAB>){
	chomp($_);
	my @tab=split("\t",$_);
	my $id_pcent=$tab[2];
	my $e_value=$tab[10];
	my $score=$tab[11];
	if (($score>$score_th) && ($e_value<$evalue_th) && ($id_pcent>$pcent_th)){
		$tab[1]=~/^\d*_(.*)_(gene_\d*)/;
		my $id_contig=$1;
		if ($id_contig=~/(.*)_part1$/){
			$id_contig=$1;
		}
		my $id_prot=$1."-".$2;
		if (($id_contig eq $contig_code) && (defined($coord_prot{$id_prot}))){
			$i++;
			my $id="OMZ_other";
			my $id_read=$tab[0];
			$hits{$id}{$i}{"id_read"}=$id_read;
			$hits{$id}{$i}{"id_pcent"}=$tab[2];
			if (!defined($check{$id})){
				print "on check $id \n";
				$check{$id}=1;
			}
			if ($coord_prot{$id_prot}{"strand"}==1){
				$hits{$id}{$i}{"start"}=$coord_prot{$id_prot}{"start"}+($tab[8]*3);
				$hits{$id}{$i}{"stop"}=$coord_prot{$id_prot}{"start"}+($tab[9]*3);
			}
			elsif ($coord_prot{$id_prot}{"strand"}==-1){
				$hits{$id}{$i}{"start"}=$coord_prot{$id_prot}{"stop"}-($tab[9]*3);
				$hits{$id}{$i}{"stop"}=$coord_prot{$id_prot}{"stop"}-($tab[8]*3);
			}
			else{print "$id_prot !!!!!!!!!!!!!!!!!!!!!\n"}
		}
	}
}
close TAB;
print "omz done\n";


open(MICROBES,"<$blast_file_microbial_metagenome") || die ("pblm opening file $blast_file_microbial_metagenome\n");
while (<MICROBES>){
	chomp($_);
	my @tab=split("\t",$_);
	my $id_pcent=$tab[2];
	my $e_value=$tab[10];
	my $score=$tab[11];
	if (($score>$score_th) && ($e_value<$evalue_th) && ($id_pcent>$pcent_th)){
 		$tab[1]=~/^\d*_(.*)_(gene_\d*)/;
		my $id_contig=$1;
		if ($id_contig=~/(.*)_part1$/){
			$id_contig=$1;
		}
		my $id_prot=$1."-".$2;
		if (($id_contig eq $contig_code) && (defined($coord_prot{$id_prot}))){
			$i++;
			my $query=$tab[0];
			my $id="OMZ_Saanich";
			my $id_read=$tab[0];
			$hits{$id}{$i}{"id_read"}=$id_read;
			$hits{$id}{$i}{"id_pcent"}=$tab[2];
			if (!defined($check{$id})){
				print "on check $id \n";
				$check{$id}=1;
			}
			if ($coord_prot{$id_prot}{"strand"}==1){
				$hits{$id}{$i}{"start"}=$coord_prot{$id_prot}{"start"}+($tab[8]*3);
				$hits{$id}{$i}{"stop"}=$coord_prot{$id_prot}{"start"}+($tab[9]*3);
			}
			elsif ($coord_prot{$id_prot}{"strand"}==-1){
				$hits{$id}{$i}{"start"}=$coord_prot{$id_prot}{"stop"}-($tab[9]*3);
				$hits{$id}{$i}{"stop"}=$coord_prot{$id_prot}{"stop"}-($tab[8]*3);
			}
		}
	}
}
close MICROBES;
print "microbes done\n";


open(TAB,"<$blast_file_tblastx_photic") || die "pblm ouverture fichier $blast_file_tblastx_photic\n";
while(<TAB>){
	chomp($_);
	my @tab=split("\t",$_);
	my $id_pcent=$tab[2];
	my $e_value=$tab[10];
	my $score=$tab[11];
	if (($score>$score_th) && ($e_value<$evalue_th) && ($id_pcent>$pcent_th)){
		$tab[1]=~/^\d*_(.*)/;
		my $id_contig=$1;
		if ($id_contig=~/(.*)_part1$/){
			$id_contig=$1;
		}
		my $id_prot=$1."-".$2;
		if ($id_contig eq $contig_code){
			$i++;
			my $id="photic";
			my $id_read=$tab[0];
			$hits{$id}{$i}{"id_read"}=$id_read;
			$hits{$id}{$i}{"id_pcent"}=$tab[2];
			if (!defined($check{$id})){
				print "on check $id \n";
				$check{$id}=1;
			}
			if ($tab[9]>$tab[8]){ # sens
				$hits{$id}{$i}{"start"}=$tab[8];
				$hits{$id}{$i}{"stop"}=$tab[9];
			}
			else{ # complement
				$hits{$id}{$i}{"start"}=$tab[9];
				$hits{$id}{$i}{"stop"}=$tab[8];
			}
		}
	}
}
close TAB;
print "tblastx photic done \n";


open(TAB,"<$blast_file_tblastx_aphotic") || die "pblm ouverture fichier $blast_file_tblastx_aphotic\n";
while(<TAB>){
	chomp($_);
	my @tab=split("\t",$_);
	my $id_pcent=$tab[2];
	my $e_value=$tab[10];
	my $score=$tab[11];
	if (($score>$score_th) && ($e_value<$evalue_th) && ($id_pcent>$pcent_th)){
 		$tab[1]=~/^\d*_(.*)/;
		my $id_contig=$1;
		if ($id_contig=~/(.*)_part1$/){
			$id_contig=$1;
		}
		my $id_prot=$1."-".$2;
		if ($id_contig eq $contig_code){
			$i++;
			my $id="aphotic";
			my $id_read=$tab[0];
			$hits{$id}{$i}{"id_read"}=$id_read;
			$hits{$id}{$i}{"id_pcent"}=$tab[2];
			if (!defined($check{$id})){
				print "on check $id \n";
				$check{$id}=1;
			}
			if ($tab[9]>$tab[8]){ # sens
				$hits{$id}{$i}{"start"}=$tab[8];
				$hits{$id}{$i}{"stop"}=$tab[9];
			}
			else{ # complement
				$hits{$id}{$i}{"start"}=$tab[9];
				$hits{$id}{$i}{"stop"}=$tab[8];
			}
		}
	}
}
close TAB;
print "tblastx aphotic done \n";

open(TAB,"<$blast_file_tblastx_omz") || die "pblm ouverture fichier $blast_file_tblastx_omz\n";
while(<TAB>){
	chomp($_);
	my @tab=split("\t",$_);
	my $id_pcent=$tab[2];
	my $e_value=$tab[10];
	my $score=$tab[11];
	if (($score>$score_th) && ($e_value<$evalue_th) && ($id_pcent>$pcent_th)){
 		$tab[1]=~/^\d*_(.*)/;
		my $id_contig=$1;
		if ($id_contig=~/(.*)_part1$/){
			$id_contig=$1;
		}
		my $id_prot=$1."-".$2;
		if ($id_contig eq $contig_code){
			$i++;
			my $id="OMZ_other";
			my $id_read=$tab[0];
			$hits{$id}{$i}{"id_read"}=$id_read;
			$hits{$id}{$i}{"id_pcent"}=$tab[2];
			if (!defined($check{$id})){
				print "on check $id \n";
				$check{$id}=1;
			}
			if ($tab[9]>$tab[8]){ # sens
				$hits{$id}{$i}{"start"}=$tab[8];
				$hits{$id}{$i}{"stop"}=$tab[9];
			}
			else{ # complement
				$hits{$id}{$i}{"start"}=$tab[9];
				$hits{$id}{$i}{"stop"}=$tab[8];
			}
		}
	}
}
close TAB;
print "tblastx omz done \n";


my %max;
foreach(keys %hits){
	my $id_virome=$_;
	my @t=keys %{$hits{$id_virome}};
	my $n=$#t+1;
	print "$id_virome - $n hits total\n";
}

open(S1,">$out_file") || die "pblm ouverture fichier $out_file\n";
print S1 "hit_number,read,virome,deb,fin,id,milieu,type\n";
foreach(keys %hits){
	my $id_virome=$_;
	foreach(keys %{$hits{$id_virome}}){
		my $middle=($hits{$id_virome}{$_}{"start"}+$hits{$id_virome}{$_}{"stop"})/2;
		if($middle>$contig_size{$contig_code}){}
		else{
			print S1 "hit-$_,$hits{$id_virome}{$_}{id_read},$id_virome,$hits{$id_virome}{$_}{start},$hits{$id_virome}{$_}{stop},$hits{$id_virome}{$_}{id_pcent},$middle,$hits{$id_virome}{$_}{type_dataset}\n";
		}
		if (($hits{$id_virome}{$_}{"id_pcent"}>80)){
			# We compute the coverage if percentage id > 80 %
			for (my $j=$hits{$id_virome}{$_}{"start"};$j<=$hits{$id_virome}{$_}{"stop"};$j++){
				if (defined($depth{$hits{$id_virome}{$_}{"id_read"}})){
					$coverage{$j}{$id_virome}+=$depth{$hits{$id_virome}{$_}{"id_read"}};
				}
				else{
					$coverage{$j}{$id_virome}+=1;
				}
			}
		}
	}
}
close S1;


# Now we compute the coverage, smoothed over a sliding window
my %smoothed_coverage;
my $step=1;
my $window=2500;
if ($contig_code=~/D02C10/){
# The sliding window is smaller for the Microviridae genome
	$window=500;
	$step=1;
}
my %average_coverage;
my %total_count;
my $nb_window=0;
for(my $i=1;$i<=$contig_size{$contig_code};$i+=$step){
	if ((($i-1) % 1000)== 0){print "$i\n";}
	my $n=0;
	for (my $j=$i-$window;$j<=$i+$window;$j++){
		if (defined($coverage{$j})){
			$n++;
			foreach(@liste_category){
				$smoothed_coverage{$i}{$_}+=$coverage{$j}{$_};
			}
		}
	}
	foreach(@liste_category){
		$smoothed_coverage{$i}{$_}/=$n;
		$average_coverage{$_}+=$smoothed_coverage{$i}{$_};
		if ($smoothed_coverage{$i}{$_}==0){$smoothed_coverage{$i}{$_}=-2;}
		else{$smoothed_coverage{$i}{$_}=(log($smoothed_coverage{$i}{$_})/log(10));}
	}
	$nb_window++;
}
print "Average coverage (before log) : \n";
foreach(@liste_category){
	my $t=$average_coverage{$_}/$nb_window;
	print "$_ - $t\n";
}

open(S2,">$out_file_coverage") || die "pblm opening file $out_file_coverage";
print S2 "coordinate,coverage,virome";
print S2 "\n";
for(my $i=1;$i<=$contig_size{$contig_code};$i+=$step){
	foreach(@liste_category){
		if ($smoothed_coverage{$i}{$_}<-2){$smoothed_coverage{$i}{$_}=-2;}
		print S2 "$i,$smoothed_coverage{$i}{$_},$_\n";
	}
}
close S2;
