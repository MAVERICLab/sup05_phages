#!/usr/bin/perl
use strict;
# Script to select all contigs putatively viral that would not have similarity to a known viral genome
# Argument 0 : csv file with each gene affiliation
# Argument 1 : out csv file

if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[1])))
{
	print "# Script to select all contigs of interest 
# Argument 0 : csv file with each gene affiliation
# Argument 1 : out csv file \n";
	die "\n";
}


my $csv_file=$ARGV[0];
my $out_file=$ARGV[1];

# Files available through Metavir (http://metavir-meb.univ-bpclermont.fr) : Project 2948
my $fasta_base="Seqs_nett/2948/2948_whole.fasta";
my $fasta_base_prot="Seqs_nett/2948/2948_prot.fasta";

# We will look at 10 gene windows, and look for regions with more than 50% of uncharacterized genes, and / or more than 80% of genes coded on the same strand
my $window=10;
my $ratio_uncharacterized=0.5;
my $ratio_same_sens=0.8;


# Get the size and number of gene predicted for each sequence
open(FA,"<$fasta_base") || die "pblm opening $fasta_base\n";
my $id_c="";
my %size;
while(<FA>){
	chomp($_);
	if ($_=~/^>(.*)/){
		$id_c=$1;
	}
	else{
		$size{$id_c}+=length($_);
	}
}
close FA;

open(PROT,"<$fasta_base_prot") || die "pblm opening $fasta_base_prot\n";
my %nb_prot;
while(<PROT>){
	chomp($_);
	if ($_=~/^>\d*_(.*)-gene_.*/){
		my $id=$1;
		$nb_prot{$id}++;
	}
}
close PROT;

my %infos;
my $contig_c="";
my $gene_num=0;
# Parse the automatic annotation given by Metavir
open(F1,"<$csv_file") || die "pblm ouverture fichier $csv_file\n";
while(<F1>){
	chomp($_);
	if ($_=~/^Contig/){}
	elsif($_=~/^,/){
		my @tab=split(',',$_);
		if (!defined($tab[10])){
				print "!!!!!!!!!!!!!!!!!!! pblm with line $_\n";
			<STDIN>;
		}
		$tab[10]=~s/\"//g;
		$tab[5]=~s/\"//g;
		$infos{$contig_c}{$tab[1]}{"order"}=$gene_num;
		$infos{$contig_c}{$tab[1]}{"sens"}=$tab[5];
		$infos{$contig_c}{$tab[1]}{"product"}=$tab[10];
		$gene_num++;
	}
	else{
		$contig_c=$_;
		$gene_num=0;
	}
}
close F1;

# Summarize results in a tab file
my %check;
my $tag=-1;
open(S1,">$out_file") || die ("pblm opening file $out_file\n");
print S1 "Contig|Size|Gene_start|Gene_end|Ratio_no_pfam|Ratio_sens|Ratio_reverse\n";
foreach(sort keys %infos){
	$contig_c=$_;
	$contig_c=~/^\d*_(.*)/;
	my $contig_num=$1;
	$tag=-1;
	# For each gene
	my @liste_genes=sort {$infos{$contig_c}{$a}{"order"} <=> $infos{$contig_c}{$b}{"order"}} keys %{$infos{$contig_c}};
	for (my $i=0;$i<=$#liste_genes-$window;$i++){
		# looking at the 10-gene window 
		my %compte;
		for (my $j=$i;$j<$i+$window;$j++){
			if ($infos{$contig_c}{$liste_genes[$j]}{"sens"} eq "-"){$compte{"reverse"}++;}
			if ($infos{$contig_c}{$liste_genes[$j]}{"product"} eq "- -"){$compte{"no"}++;}
		}
		$compte{"no"}/=$window;
		$compte{"reverse"}/=$window;
		$compte{"sens"}=1-$compte{"reverse"};
		if (($compte{"no"}>=$ratio_uncharacterized) && (($compte{"reverse"}>=$ratio_same_sens) || ($compte{"sens"}>=$ratio_same_sens))){
			if ($tag==-1){
				$tag=$i;
			}
		}
		elsif($tag!=-1){
			my %compte;
			my $last=$i+$window-1;
			for (my $j=$tag;$j<$last;$j++){
				if ($infos{$contig_c}{$liste_genes[$j]}{"sens"} eq "-"){$compte{"reverse"}++;}
				if ($infos{$contig_c}{$liste_genes[$j]}{"product"} eq "- -"){$compte{"no"}++;}
			}
			$compte{"no"}/=$window;
			$compte{"reverse"}/=$window;
			$compte{"sens"}=1-$compte{"reverse"};
			print S1 "$contig_c|$size{$contig_num}|$liste_genes[$tag]|$liste_genes[$i+$window]|$compte{no}|$compte{sens}|$compte{reverse}\n";
			$tag=-1;
			$check{$contig_c}++;
		}
	}
	if($tag!=-1){
		my %compte;
		my $last=$#liste_genes;
		for (my $j=$tag;$j<$last;$j++){
			if ($infos{$contig_c}{$liste_genes[$j]}{"sens"} eq "-"){$compte{"reverse"}++;}
			if ($infos{$contig_c}{$liste_genes[$j]}{"product"} eq "- -"){$compte{"no"}++;}
		}
		$compte{"no"}/=$window;
		$compte{"reverse"}/=$window;
		$compte{"sens"}=1-$compte{"reverse"};
		print S1 "$contig_c|$size{$contig_num}|$liste_genes[$tag]|$liste_genes[$last]|$compte{no}|$compte{sens}|$compte{reverse}\n";
		$tag=-1;
			$check{$contig_c}++;
	}
}
close S1;
my @t=keys %check;
my $n=$#t+1;
print "$n contigs matches\n";

my $last_line=join(",",@t);
print "\n\n$last_line\n";
