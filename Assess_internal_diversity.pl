#!/usr/bin/perl
use strict;
# Script to compute the value of nucleotide diversity (pi) based on mummer sequence comparison results
# Argument 0 : Fasta file of all sequences to be compared
if (($ARGV[0] eq "-h") || ($ARGV[0] eq "--h") || ($ARGV[0] eq "-help" )|| ($ARGV[0] eq "--help") || (!defined($ARGV[0])))
{
	print "# Script to compute the value of nucleotide diversity (pi) based on mummer sequence comparison results
# Argument 0 : Fasta file of all sequences to be compared
\n";
	die("\n");
}

my $tmp_1="tmp_1.fasta";
my $tmp_2="tmp_2.fasta";

my $fasta_file=$ARGV[0];
my %seq;
my $id_c;
my @liste_seq;
# Get all sequences
open(FA,"<$fasta_file") || die ("pblm opening file $fasta_file");
while (<FA>){
	chomp($_);
	if ($_=~/^>(.*)/){
		$id_c=$1;
		push(@liste_seq,$id_c);
	}
	else{
		$seq{$id_c}.=$_;
	}
}
close FA;

my $sum=0;
my $n=0;
# For each sequence
for(my $i=0;$i<=$#liste_seq;$i++){
	my $seq_1=$seq{$liste_seq[$i]};
	# Sequence 1 go in temp file 1
	open(S1,">$tmp_1") || die ("pblm file $tmp_1");
	print S1 ">$liste_seq[$i]\n$seq_1\n";
	close S1;
	for (my $j=$i+1;$j<=$#liste_seq;$j++){
		my $seq_2=$seq{$liste_seq[$j]};
		# Sequence 2 goes in temp file 2
		open(S2,">$tmp_2") || die ("pblm file $tmp_2");
		print S2 ">$liste_seq[$j]\n$seq_2\n";
		close S2;
		# Compare sequences with mummer
		`nucmer --maxgap=500 --mincluster=100 --prefix=tmp $tmp_1 $tmp_2 > out 2> out_err`;
		`show-coords -c -I 00 tmp.delta > tmp.coords`;
		my $id=0;
		# Parse mummer output to get the identity percentage between the two sequences
		open(COORDS,"<tmp.coords") || die ("pblm opening file tmp.coords");
		while (<COORDS>){
			chomp($_);
			my @tab=split(" ",$_);
			if ($tab[2] eq "|" && $tab[0] ne "[S1]"){
				if ($tab[9]>=$id){
					$id=$tab[9];
					print "$liste_seq[$i]|$liste_seq[$j]|$id\n";
				}
			}
		}
		close COORDS;
		if ($id>0){
			$sum+=$id;
			$n++;
		}
	}
}

print "$n comparison relevant total\n";
my $avg=$sum/$n;
print "which gives an average of $avg identity\n";