# Cultivation-independent exploration of SUP05 host-virus interactions in a model Oxygen Minimum Zone #

## Author ##

Simon Roux

## CITATION ##

If you use this software, please cite [Roux, S., Hawley, A.K., Torres Beltran, M., Scofield, M., Schwientek, P., Stepanauskas, R., Woyke, T., Hallam, S.J., Sullivan, M.B. (doi:10.7554/eLife.03125). Ecology and evolution of viruses infecting uncultivated SUP05 bacteria as revealed by single-cell genomics and metagenomics](http://elifesciences.org/content/3/e03125#sthash.sskQREWU.dpuf)